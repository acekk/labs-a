package pk.labs.LabA;

public class LabDescriptor {

    // region P1
    public static String displayImplClassName ="pk.labs.LabA.display.wyswietlacz";
    public static String controlPanelImplClassName = "pk.labs.LabA.controlpanel.panelKontrolny";

    public static String mainComponentSpecClassName = "pk.labs.LabA.Contracts.IMycie";
    public static String mainComponentImplClassName = "pk.labs.LabA.main.Mycie";
    // endregion

    // region P2
    public static String mainComponentBeanName = "Myjnia";
    public static String mainFrameBeanName = "ControlPanelI";
    // endregion

    // region P3
    public static String sillyFrameBeanName = "Disp";
    // endregion
}
